<?php

use app\models\ShortLink;
use yii\db\Migration;

/**
 * Handles the creation of table `short_link`.
 */
class m180704_215608_create_short_link_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(ShortLink::tableName(), [
            'id' => $this->primaryKey(),
            'code' => $this->string(),
            'redirect_url' => $this->string(),
            'expired_at' => $this->dateTime(),
        ]);

        $this->createIndex('code__unique', ShortLink::tableName(), ['code'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(ShortLink::tableName());
    }
}
