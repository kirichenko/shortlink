<?php

namespace app\controllers;

use app\forms\ShortLinkForm;
use app\models\ShortLink;
use Yii;
use yii\db\QueryBuilder;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $form = new ShortLinkForm();

        if($form->load(Yii::$app->request->post()) && $form->validate()){
            $model = new ShortLink();
            $model->code = $form->code;
            $model->redirect_url = $form->redirectUrl;
            $model->expired_at = $form->expiredAt;

            if($model->save()){
                return $this->render('index', ['form' => $form, 'model' => $model]);
            } elseif($model->hasErrors()){
                $form->addError('code', 'Короткая ссылка уже существует');
            } else {
                Yii::error('Ошибка добавления ссылки: ' . VarDumper::dumpAsString($form->attributes));
                $form->addError('code', 'Произошла неизвестная ошибка, попробуйте еще раз');
            }
        }

        return $this->render('index', ['form' => $form]);
    }

    /**
     * @param $code
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionLink($code)
    {
        /** @var ShortLink|null $shortLink */
        $shortLink = ShortLink::find()
            ->where(['code' => $code])
            ->andWhere(['or', ['>=', 'expired_at', date('Y-m-d H:i')], ['is', 'expired_at', null]])
            ->one();

        if($shortLink){
            return $this->redirect($shortLink->redirect_url);
        }

        throw new NotFoundHttpException('Short link does not exist');
    }
}
