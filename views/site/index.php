<?php
/**
 * @var \app\forms\ShortLinkForm $form
 * @var \app\models\ShortLink|null $model
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$activeForm = ActiveForm::begin() ?>

<div class="row">
    <div class="col-lg-4">

        <?= $activeForm->field($form, 'code') ?>
        <?= $activeForm->field($form, 'redirectUrl') ?>
        <?= $activeForm->field($form, 'expiredAt') ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Уменьшить') ?>
            </div>
        </div>

        <? if($model):?>
            Ссылка:
            <a href="<?= Url::toRoute(['site/link', 'code' => $model->code])?>">
                <?= Url::toRoute(['site/link', 'code' => $model->code])?>
            </a>
        <? endif?>

    </div>
</div>

<?php ActiveForm::end() ?>
