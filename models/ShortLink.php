<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\base\NotSupportedException;

/**
 * Class ShortLink
 * @package app\models
 * @property integer $id
 * @property string $code
 * @property string $redirect_url
 * @property string $expired_at
 */
class ShortLink extends ActiveRecord
{
    /**
     * @param int $length
     * @return bool|string
     */
    public static function generateCode($length = 5)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $isRetrySave = empty($this->code);
        try{
            if(empty($this->code)){
                $this->code = static::generateCode();
            }
            $result = parent::save($runValidation, $attributeNames);
        } catch (\Exception $e){
            /** Try to remove the collision */
            if($e->getCode() === static::getDuplicateCodeError()){
                if($isRetrySave){
                    $this->code = static::generateCode();
                    return parent::save($runValidation, $attributeNames);
                } else {
                    $this->addError('code', 'Duplicate code key');
                    return false;
                }
            }

            throw $e;
        }

        return $result;
    }

    /**
     * @return int
     * @throws NotSupportedException
     */
    public static function getDuplicateCodeError()
    {
        $schema = static::getDb()->getSchema();
        if($schema instanceof \yii\db\sqlite\Schema){
            return 23000;
        }
        if($schema instanceof \yii\db\mysql\Schema){
            return 1062;
        }

        throw new NotSupportedException('Undefined error code for ' . $schema::className());
    }
}
