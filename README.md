Клонируем репозиторий и переходим в папку проекта:
```sh
git clone http://bitbucket.org/kirichenko/shortlink.git
cd ./shortlink
```

Поднимаем сервис Docker и дожидаемся выполнения *composer install*
```sh
docker-compose up -d && docker-compose logs -f composer && docker-compose ps 
composer install
```

Выполняем миграции:
```
docker-compose exec php ./yii migrate
```

Стартуем встроенный в php web server 
```
docker-compose exec php ./yii serve 0.0.0.0:8080
```