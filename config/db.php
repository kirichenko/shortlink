<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:@app/db/shortLink',
    'charset' => 'utf8',
];
