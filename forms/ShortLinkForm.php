<?php

namespace app\forms;

use yii\base\Model;

/**
 * Class ShortLinkForm
 * @package app\models
 */
class ShortLinkForm extends Model
{
    /**
     * Код ссылки
     * @var string
     */
    public $code;

    /**
     * URL пользовательского редиректа с короткой ссылки
     * @var string
     */
    public $redirectUrl;

    /**
     * Последняя дата работы ссылки
     * @var string
     */
    public $expiredAt;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['redirectUrl', 'required'],
            [['code', 'redirectUrl'], 'string'],
            ['redirectUrl', 'url'],
            ['expiredAt', 'datetime',
                'format' => 'php:Y-m-d H:i',
                'message' => 'Invalid format. Example: 2018-12-10 10:00'
            ],
        ];
    }
}
